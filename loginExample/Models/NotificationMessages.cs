﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class NotificationMessages
    {
        public string Message { get; set; }
        public DateTime DateSent { get; set; }
        public int TypeID { get; set; }
        public string DescriptionType { get; set; }
        public string UserName { get; set; }
    }
}
