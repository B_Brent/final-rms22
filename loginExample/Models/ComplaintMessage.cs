﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ComplaintMessage
    {

        public int ComplaintID { get; set; }
        public int RecieverUserID { get; set; }
        public int ReporterUserID { get; set; }
        public string Title { get; set; }
        public int ComplaintTypeID { get; set; }
        public int ComplaintStatusID { get; set; }

        public string typeDesc { get; set; }

        public string statusDesc { get; set; }

        public ArrayList messsages { get; set; }


    }
}
