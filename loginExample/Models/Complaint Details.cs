﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ComplaintDetails
    {
        public int ComplaintID { get; set; }
      
        public string RecieverUserID { get; set; }
        
        public string ReporterUserID { get; set; }

        public string RecieverUserName { get; set; }

        public string ReporterUserName { get; set; }

        public string Title { get; set; }
        
        public int ComplaintTypeID { get; set; }
      
        public int ComplaintStatusID { get; set; }

        public string typeDesc { get; set; }
       
        public string statusDesc { get; set; }
        
        public int previousStatus { get; set; }

        public List<ComplaintUserMessageDetails> messages { get; set; }

    }
}
