﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class Complaint
    {
        [Key]
        public int ComplaintID { get; set; }
        public string RecieverUserID { get; set; }
        public string ReporterUserID { get; set; }
        public string Title { get; set; }
        public int ComplaintTypeID { get; set; }
        public int ComplaintStatusID { get; set; }

    }
}
