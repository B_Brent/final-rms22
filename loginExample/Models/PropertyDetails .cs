﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class PropertyDetails
    {
                  
        public int PropertyID { get; set; }
        
        public string PropertyName { get; set; }
        
        public string UserId { get; set; }
        
        public int SuburbID { get; set; }
        
        public string PropertyDescription { get; set; }
        
        public int RoomNum { get; set; }
        
        public string PropertyAddress { get; set; }

        public bool active { get; set; }

        public int size { get; set; }
        
        public int ParkNum { get; set; }
        
        public int BathNum { get; set; }

        public bool AllowPets { get; set; }
        
        public string UserName { get; set; }

        public string SuburbName { get; set; }
        
    }
}
