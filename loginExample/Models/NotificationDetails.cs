﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class NotificationDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationID { get; set; }
        public string Message { get; set; }
        public DateTime DateSent { get; set; }
        public int TypeID { get; set; }
        public String UserID { get; set; }
        public String UserName { get; set; }
        public String imageName { get; set; }

    }
}
