﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ListingStatus
    {
        [Key]
        public int ListingStatusID { get; set; }
        public string ListingStatusDescription { get; set; }
    }
}
