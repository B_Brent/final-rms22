﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class NotificationUser
    {

        public NotificationUser()
        {
           
        }

        public NotificationUser(String userID, int notificationID)
        {
            UserID = userID;
            NotificationID = notificationID;
        }

        [Key]
        public String UserID { get; set; }
        [Key]
        public int NotificationID { get; set; }
    }
}
