﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class MessageUser
    {
        public MessageUser(string userID, string userName)
        {
            UserID = userID;
            UserName = userName;
        }

        public string UserID { get; set;}

        public string UserName { get; set; }
      
    }
}
