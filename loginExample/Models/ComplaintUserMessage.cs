﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ComplaintUserMessage
    {
        [Key]
        public int ComplaintUserMessageID { get; set; }
        public string UserID { get; set; }
        public int ComplaintID { get; set; }
        public DateTime MessageDate { get; set; }
        public string Message { get; set; }

    }
}
