﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class Suburb
    {
        [Key]
        public int SuburbID { get; set; }
        public string SuburbName { get; set; }
    }
}
