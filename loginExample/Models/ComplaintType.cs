﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ComplaintType
    {
        [Key]
        public int TypeID { get; set; }
        public string Description { get; set; }       
    }
}
