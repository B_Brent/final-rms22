﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ComplaintUserMessageDetails
    {

        public ComplaintUserMessageDetails()
        {

        }

        public ComplaintUserMessageDetails(int ComplaintID)
        {
            this.ComplaintID = ComplaintID;
        }

        public int ComplaintUserMessageID { get; set; }
        public string UserID { get; set; }

        public string UserName { get; set; }
        
        public int ComplaintID { get; set; }
        
        public DateTime MessageDate { get; set; }
        
        public string Message { get; set; }
    }
}
