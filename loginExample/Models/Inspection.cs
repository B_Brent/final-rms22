﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class Inspection
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InspectionID { get; set; }
        public int PropertyID { get; set; }
        public string Details { get; set; }
        public bool InspectionStatus { get; set; }
        public DateTime InspectionDate { get; set; }
        public bool Active { get; set; }
    }
}
