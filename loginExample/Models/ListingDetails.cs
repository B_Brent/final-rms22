﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class ListingDetails
    {
        [Key]
        public int ListID { get; set; }
        public int ListingStatusID { get; set; }
        public double ListingPrice { get; set; }

        public DateTime AvailabilityDate { get; set; }
        public int PropertyID { get; set; }

        public string ListingStatusDescription { get; set; }

        public string PropertyName { get; set; }

    }
}
