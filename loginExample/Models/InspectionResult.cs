﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class InspectionResult
    {
        [Key]
        public int ResultID { get; set; }
        public string Description { get; set; }
     
    }
}
