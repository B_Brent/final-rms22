﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace loginExample.Models
{
    public class PropertyImage
    {
        [Key]
        public int ImageID { get; set; }
        public byte[] Image { get; set; }
        public int PropertyID { get; set; }
    }
}
