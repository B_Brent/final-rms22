﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using System.Text;
using HtmlAgilityPack;
using System.IO;


namespace loginExample.Controllers
{
    
    [Authorize]
    public class InspectionController : Controller
    {
        private readonly ILogger<InspectionController> _logger;
        private readonly ApplicationDbContext _db;
        

        public InspectionController(ApplicationDbContext db, ILogger<InspectionController> logger)
        {
            _db = db;
            _logger = logger;
        }

        [AllowAnonymous]
        public  IActionResult Index()
        {
          
            string userId = User.Identity.GetUserId();
            var inspections = new List<InspectionDetails>();
            var properties = new List<PropertyDetails>();

            if (User.IsInRole("admin"))
            {
                ViewBag.properties = _db.Property.Where(p => p.active).ToList();
                inspections = (from i in _db.Inspection
                               join p in _db.Property on i.PropertyID equals p.PropertyID
                               where i.Active
                               select new InspectionDetails
                               {
                                   Active = i.Active,
                                   Details = i.Details,
                                   InspectionDate = i.InspectionDate,
                                   InspectionID = i.InspectionID,
                                   InspectionStatus = i.InspectionStatus,
                                   PropertyID = i.PropertyID,
                                   PropertyName = p.PropertyName

                               }).ToList();
            }

            else if (User.IsInRole("owner"))
            {
                ViewBag.properties = _db.Property.Where(p => p.active && p.UserId == userId).ToList();
                inspections = (from i in _db.Inspection
                               join p in _db.Property on i.PropertyID equals p.PropertyID
                               where i.Active && p.UserId == userId
                               select new InspectionDetails
                               {
                                   Active = i.Active,
                                   Details = i.Details,
                                   InspectionDate = i.InspectionDate,
                                   InspectionID = i.InspectionID,
                                   InspectionStatus = i.InspectionStatus,
                                   PropertyID = i.PropertyID,
                                   PropertyName = p.PropertyName

                               }).ToList();
            }
            else {

                List<int> propertyIds = (from r in _db.Rental                                 
                                  where r.UserID == userId && r.EndDate >= new DateTime()
                                  select  r.PropertyID

                                  ).ToList(); 
                

                if(propertyIds.Count > 0) {
                    inspections = (from i in _db.Inspection
                                   join p in _db.Property on i.PropertyID equals p.PropertyID
                                   where i.Active && propertyIds.Contains(p.PropertyID)
                                   select new InspectionDetails
                                   {
                                       Active = i.Active,
                                       Details = i.Details,
                                       InspectionDate = i.InspectionDate,
                                       InspectionID = i.InspectionID,
                                       InspectionStatus = i.InspectionStatus,
                                       PropertyID = i.PropertyID,
                                       PropertyName = p.PropertyName

                                   }).ToList();
                }

            }
            return View(inspections);

        }

        [HttpPost]
        public FileResult Export(string GridHtml)
        {
            if(GridHtml == null)
            {
                GridHtml = "No Data Available to show";
            }

            HtmlNode.ElementsFlags["img"] = HtmlElementFlag.Closed;
            HtmlNode.ElementsFlags["input"] = HtmlElementFlag.Closed;
            HtmlDocument doc = new HtmlDocument();
            doc.OptionFixNestedTags = true;
            doc.LoadHtml(GridHtml);
            GridHtml = doc.DocumentNode.OuterHtml;

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                Encoding unicode = Encoding.UTF8;
                StringReader sr = new StringReader(GridHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "ScheduledInspections.pdf");
            }
        }


        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Upsert(InspectionDetails inspectionDetails, Inspection inspection)
        {
            if (inspectionDetails.InspectionID == 0)
            {
                var inspectionCount = _db.Inspection.Count(i => i.PropertyID.Equals(inspectionDetails.PropertyID));
                
                if (inspectionCount == 0)
                {
                    inspection.Active = true;
                    await _db.Inspection.AddAsync(inspection);
                    await _db.SaveChangesAsync();
                    //ViewBag.Message = "Added New User Successfully";
                    return RedirectToAction("Index");
                }
                
                else
                {
                    return RedirectToAction("Index");
                    // name already exists
                }
            }

            else
            {
                inspection.Active = true;
                _db.Inspection.Update(inspection);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
                //  ViewBag.Message = "Updated User Details Successfully";
            }

        }

        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Delete(Inspection inspection)
        {
            inspection.Active = false;
            _db.Inspection.Attach(inspection).Property(i => i.Active).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [AllowAnonymous]
        public IActionResult InspectionItems(int InspectionID)
        {
            ViewBag.inspectionResult = _db.InspectionResult.ToList();

            var inspectionItems = (from i in _db.InspectionItem
                                    join r in _db.InspectionResult on i.ResultID equals r.ResultID
                                    where (i.Active && i.InspectionID == InspectionID)
                                    select new InspectionItemDetails
                                    {
                                        ItemID = i.ItemID,
                                        Active = i.Active,
                                        InspectionID = i.InspectionID,
                                        ResultID = i.ResultID,
                                        Name = i.Name,
                                        Notes = i.Notes,
                                        Description = i.Description,
                                        resultDescription = r.Description
                                    }).ToList();

            return View(inspectionItems);

        }


        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> UpsertItem(InspectionItemDetails inspectionItemDetails, InspectionItem inspectionItem)
        {
            if (inspectionItemDetails.ItemID == 0)
            {
                
                inspectionItem.Active = true;
                await _db.InspectionItem.AddAsync(inspectionItem);
                await _db.SaveChangesAsync();

                return RedirectToAction("InspectionItems", new { InspectionID = inspectionItem.InspectionID });
            }

            else
            {
                inspectionItem.Active = true;
                _db.InspectionItem.Update(inspectionItem);
                await _db.SaveChangesAsync();
                return RedirectToAction("InspectionItems", new { InspectionID = inspectionItem.InspectionID });
              
            }

        }

        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> DeleteItem(InspectionItem inspectionItem)
        {
            inspectionItem.Active = false;
            _db.InspectionItem.Attach(inspectionItem).Property(i => i.Active).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("InspectionItems", new { InspectionID = inspectionItem.InspectionID });
        }

    }
}
