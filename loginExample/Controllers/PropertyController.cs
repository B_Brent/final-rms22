﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;

namespace loginExample.Controllers
{ 
    [Authorize]
    public class PropertyController : Controller
    {
        private readonly ILogger<PropertyController> _logger;
        private readonly ApplicationDbContext _db;

      
        public PropertyController(ApplicationDbContext db, ILogger<PropertyController> logger)
        {
            _db = db;
            _logger = logger;
        }

        [Authorize(Roles = "admin, owner")]
        public  IActionResult Index()
        {
            
            string userId = User.Identity.GetUserId();
            var properties = new List<PropertyDetails>();


            ViewBag.suburbs = _db.Suburb.ToList();
            ViewBag.owners = new List<User>();

            if (User.IsInRole("admin"))
            {
                ViewBag.owners = (from u in _db.Users
                                  join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                                  join r in _db.Roles on user_roles.RoleId equals r.Id
                                  where r.Id != "4"
                                  orderby u.UserName
                                  select u
                                  ).ToList();

                properties = (from p in _db.Property
                              join s in _db.Suburb on p.SuburbID equals s.SuburbID
                              join u in _db.Users on p.UserId equals u.Id
                              orderby p.PropertyName
                              where p.active == true
                              select new PropertyDetails
                              {
                                  UserName = u.UserName,
                                  PropertyName = p.PropertyName,
                                  PropertyID = p.PropertyID,
                                  PropertyAddress = p.PropertyAddress,
                                  PropertyDescription = p.PropertyDescription,
                                  RoomNum = p.RoomNum,
                                  SuburbID = p.SuburbID,
                                  UserId = p.UserId,
                                  active = p.active,
                                  AllowPets = p.AllowPets,
                                  BathNum = p.BathNum,
                                  size = p.size,
                                  ParkNum = p.ParkNum,
                                  SuburbName = s.SuburbName
                              }).ToList();
            }
            else {
                ViewBag.owners = _db.Users.Where(u => u.Id == userId).ToList();

                properties = (from p in _db.Property
                              join s in _db.Suburb on p.SuburbID equals s.SuburbID
                              join u in _db.Users on p.UserId equals u.Id
                              orderby p.PropertyName
                              where p.active == true && p.UserId == userId
                              select new PropertyDetails
                              {
                                  UserName = u.UserName,
                                  PropertyName = p.PropertyName,
                                  PropertyID = p.PropertyID,
                                  PropertyAddress = p.PropertyAddress,
                                  PropertyDescription = p.PropertyDescription,
                                  RoomNum = p.RoomNum,
                                  SuburbID = p.SuburbID,
                                  UserId = p.UserId,
                                  active = p.active,
                                  AllowPets = p.AllowPets,
                                  BathNum = p.BathNum,
                                  size = p.size, 
                                  ParkNum = p.ParkNum,
                                  SuburbName = s.SuburbName
                              }).ToList();
            }

            return View(properties);
        }


        [Authorize(Roles = "admin, owner, tenant")]
        public IActionResult Rules()
        {
            return View();
        }


        [Authorize(Roles = "admin, owner, tenant")]
        public IActionResult Contacts()
        {
            return View();
        }


        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Upsert(PropertyDetails propertyDetails, Property property, PropertyImage propertyImage, IFormFileCollection files)
        {

            if (property.PropertyID == 0)
            {

                var propertyCount = _db.Property.Count(p => p.PropertyName.Equals(property.PropertyName));

                if (propertyCount == 0)
                {
                    property.active = true;
                    await _db.Property.AddAsync(property);
                    await _db.SaveChangesAsync();
                  
                }
                else
                {
                    return RedirectToAction("Index");
                    // name already exists
                }
            }
            else
            {
                property.active = true;
                _db.Property.Update(property);
                await _db.SaveChangesAsync();
                //  ViewBag.Message = "Updated User Details Successfully";
            }


            if (files != null)
            {
                foreach (IFormFile file in files)
                {
                    PropertyImage image = new PropertyImage();
                    image.PropertyID = property.PropertyID;
                   
                    using (var readStream = file.OpenReadStream())
                    using (var memoryStream = new MemoryStream())
                    {
                        readStream.CopyTo(memoryStream);
                        image.Image = memoryStream.ToArray();
                    }

                    await _db.PropertyImage.AddAsync(image);
                    await _db.SaveChangesAsync();
                }
            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Delete(Property property)
        {
            property.active = false;
            _db.Property.Attach(property).Property(p => p.active).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        public IActionResult Images(int PropertyID)
        {
           var images = _db.PropertyImage.Where(i => i.PropertyID == PropertyID).ToList();
         
           ViewBag.properyIDImage = PropertyID; 
            return View(images);

        }


        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> AddImage(int properyID, IFormFileCollection files) 
        {

            if (files != null)
            {
                foreach (IFormFile file in files)
                {
                    PropertyImage image = new PropertyImage();
                    image.PropertyID = properyID;

                    using (var readStream = file.OpenReadStream())
                    using (var memoryStream = new MemoryStream())
                    {
                        readStream.CopyTo(memoryStream);
                        image.Image = memoryStream.ToArray();
                    }

                    await _db.PropertyImage.AddAsync(image);
                    await _db.SaveChangesAsync();
                }
            }

            return RedirectToAction("Images", new { PropertyID = properyID });
        }

        
        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> DeleteImage(int ImageID)
        {
            PropertyImage image = _db.PropertyImage.Find(ImageID);

            if (image != null) {
                _db.PropertyImage.Remove(image);
                await _db.SaveChangesAsync();
            }

            return RedirectToAction("Images", new { PropertyID = image.PropertyID });
       
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
