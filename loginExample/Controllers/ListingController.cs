﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;

namespace loginExample.Controllers
{ 
    [Authorize]
    public class ListingController : Controller
    {
        private readonly ILogger<ListingController> _logger;
        private readonly ApplicationDbContext _db;

      
        public ListingController(ApplicationDbContext db, ILogger<ListingController> logger)
        {
            _db = db;
            _logger = logger;
        }

        [Authorize(Roles = "admin, owner")]
        public  IActionResult Index()
        {

            string userId = User.Identity.GetUserId();
            
            var listings = new List<ListingDetails>();
            ViewBag.propertyID = new List<Property>();


            ViewBag.listingStatusDescription = _db.ListingStatus.ToList();

            if (User.IsInRole("admin")) {

                ViewBag.propertyID = _db.Property.Where(p => p.active).ToList();

                listings = (from l in _db.Listing
                                join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                                join p in _db.Property on l.PropertyID equals p.PropertyID
                                orderby l.AvailabilityDate
                                where l.ListingStatusID != 3 && p.active
                            select new ListingDetails
                                {
                                    ListID = l.ListID,
                                    ListingPrice = l.ListingPrice,
                                    AvailabilityDate = l.AvailabilityDate,
                                    PropertyID = l.PropertyID,
                                    ListingStatusID = l.ListingStatusID,
                                    ListingStatusDescription = ls.ListingStatusDescription,
                                    PropertyName = p.PropertyName
                                }).ToList();
            }
            else {
                ViewBag.propertyID = _db.Property.Where(p => p.active && p.UserId == userId).ToList();

                listings = (from l in _db.Listing
                            join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                            join p in _db.Property on l.PropertyID equals p.PropertyID
                            orderby l.AvailabilityDate
                            where l.ListingStatusID != 3 && p.UserId == userId && p.active
                            select new ListingDetails
                            {
                                ListID = l.ListID,
                                ListingPrice = l.ListingPrice,
                                AvailabilityDate = l.AvailabilityDate,
                                PropertyID = l.PropertyID,
                                ListingStatusID = l.ListingStatusID,
                                ListingStatusDescription = ls.ListingStatusDescription,
                                PropertyName = p.PropertyName
                            }).ToList();

            }

            return View(listings);
        }


        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Upsert(ListingDetails listingDetails, Listing listing)
        {

            if (listing.ListID == 0)
            {

                await _db.Listing.AddAsync(listing);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                _db.Listing.Update(listing);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
        }

        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Delete(Listing listing)
        {
            listing.ListingStatusID = 3;
            _db.Listing.Attach(listing).Property(l => l.ListingStatusID).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
