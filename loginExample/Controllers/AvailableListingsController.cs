﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;

namespace loginExample.Controllers
{
    public class AvailableListingsController : Controller
    {

        private readonly ILogger<AvailableListingsController> _logger;
        private readonly ApplicationDbContext _db;

        public AvailableListingsController(ApplicationDbContext db, ILogger<AvailableListingsController> logger)
        {
            _db = db;
            _logger = logger;
        }

        public ActionResult Index(int suburbFilter = -1, int minPriceFilter = -1, int maxPriceFilter = Int16.MaxValue, int bedroomFilter = -1)
        {

            ViewBag.bedroomFilter = bedroomFilter;
            ViewBag.minPriceFilter = minPriceFilter;
            ViewBag.maxPriceFilter = maxPriceFilter;
            ViewBag.bedroomFilter = bedroomFilter;
            ViewBag.suburbFilter = suburbFilter;


            ViewBag.suburbs = _db.Suburb.ToList();

            var listings = (from l in _db.Listing
                            join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                            join p in _db.Property on l.PropertyID equals p.PropertyID
                            join u in _db.Users on p.UserId equals u.Id
                            join s in _db.Suburb on p.SuburbID equals s.SuburbID
                            orderby l.AvailabilityDate
                            where l.ListingStatusID == 1 && p.active && p.RoomNum >= bedroomFilter && l.ListingPrice >= minPriceFilter && l.ListingPrice <= maxPriceFilter && (suburbFilter > -1 ? p.SuburbID == suburbFilter : true)
                            select new AvailableListings
                            {
                                ListID = l.ListID,
                                PropertyID = p.PropertyID,
                                ListingPrice = l.ListingPrice,
                                AvailabilityDate = l.AvailabilityDate,
                                ListingStatusDescription = ls.ListingStatusDescription,
                                PropertyName = p.PropertyName,
                                PropertyAddress = p.PropertyAddress,
                                RoomNum = p.RoomNum,
                                PropertyDescription = p.PropertyDescription,
                                SuburbName = s.SuburbName,
                                UserName = u.UserName,
                                PhoneNumber = u.PhoneNumber,
                                Email = u.Email,
                                size = p.size,
                                ParkNum = p.ParkNum,
                                AllowPets = p.AllowPets,
                                BathNum = p.BathNum,
                            }).ToList();

            foreach (AvailableListings l in listings) {
                l.Images = _db.PropertyImage.Where(i => i.PropertyID == l.PropertyID ).ToList();
            }
     
            return View(listings);
        }


        [HttpGet]
        public async Task<IActionResult> GetAvailableListing()
        {
            var listings = await (from l in _db.Listing
                                  join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                                  join p in _db.Property on l.PropertyID equals p.PropertyID
                                  join u in _db.Users on p.UserId equals u.Id
                                  join s in _db.Suburb on p.SuburbID equals s.SuburbID
                                  orderby l.AvailabilityDate
                                  where l.ListingStatusID == 1
                                  select new
                                  {
                                      l.ListID,
                                      l.ListingPrice,
                                      l.AvailabilityDate,
                                      l.PropertyID,
                                      ls.ListingStatusDescription,
                                      p.PropertyName,
                                      p.PropertyAddress,
                                      p.RoomNum,
                                      s.SuburbName,
                                      u.UserName,
                                      u.PhoneNumber,
                                      u.Email
                                  }).ToListAsync();

            return Json(new { data = listings });
        }
    }
}