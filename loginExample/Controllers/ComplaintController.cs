﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;


namespace loginExample.Controllers
{
    
    [Authorize]
    public class ComplaintController : Controller
    {
        private readonly ILogger<ComplaintController> _logger;
        private readonly ApplicationDbContext _db;
        

        public ComplaintController(ApplicationDbContext db, ILogger<ComplaintController> logger)
        {
            _db = db;
            _logger = logger;
        }

       [AllowAnonymous]
        public  IActionResult Index(string fromFilter= "default", int typeFilter = -1, int statusFilter = -1)
        {

            string userId = User.Identity.GetUserId();
            ViewBag.statusFilter = statusFilter;
            ViewBag.typeFilter = typeFilter;
            ViewBag.fromFilter = fromFilter;
            ViewBag.userId = userId;

            ViewBag.users = (from u in _db.Users
                             join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                             join r in _db.Roles on user_roles.RoleId equals r.Id
                             where r.Id != "4"
                             orderby u.UserName
                             select u
                             ).ToList();

            ViewBag.status = _db.ComplaintStatus.ToList();
            ViewBag.types = _db.ComplaintType.ToList();


            var complaints = (from c in _db.Complaint
                              join t in _db.ComplaintType on c.ComplaintTypeID equals t.TypeID
                              join s in _db.ComplaintStatus on c.ComplaintStatusID equals s.StatusID
                              where (c.RecieverUserID == userId || c.ReporterUserID == userId) && (fromFilter != "default" ? c.ReporterUserID == fromFilter : true) && (typeFilter > -1 ? t.TypeID == typeFilter : true) && (statusFilter > -1 ? s.StatusID == statusFilter : true)
                              select new ComplaintDetails
                              {
                                  ComplaintID = c.ComplaintID,
                                  ReporterUserID = c.ReporterUserID,
                                  ReporterUserName = c.ReporterUserID, 
                                  Title = c.Title,
                                  ComplaintStatusID = c.ComplaintStatusID,
                                  RecieverUserID = c.RecieverUserID,
                                  RecieverUserName = c.RecieverUserID, 
                                  ComplaintTypeID = c.ComplaintTypeID,
                                  typeDesc = t.Description,
                                  statusDesc = s.Description,
                                  previousStatus = c.ComplaintStatusID
                              }).ToList();

            foreach (ComplaintDetails complaintDetails in complaints)
            {

                IdentityUser reporter = _db.Users.Find(complaintDetails.ReporterUserID);
                IdentityUser reciever = _db.Users.Find(complaintDetails.RecieverUserID);

                complaintDetails.ReporterUserName = reporter != null ? reporter.UserName : "";
                complaintDetails.RecieverUserName = reciever != null ? reciever.UserName : "";

                complaintDetails.messages = (from m in _db.ComplaintUserMessage
                                             join u in _db.Users on m.UserID equals u.Id
                                             where (m.ComplaintID == complaintDetails.ComplaintID)
                                             select new ComplaintUserMessageDetails
                                             {
                                                 ComplaintID = m.ComplaintID,
                                                 Message = m.Message,
                                                 MessageDate = m.MessageDate,
                                                 ComplaintUserMessageID = m.ComplaintUserMessageID,
                                                 UserID = m.UserID,
                                                 UserName = u.UserName
                                             }).ToList();

            }

            return View(complaints);
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Upsert(ComplaintDetails complaintDetails, Complaint complaint, ComplaintUserMessage complaintUserMessage)
        {

            if (complaint.ComplaintID == 0)
            {

                // adds complaint
                string userId = User.Identity.GetUserId();
                complaint.ReporterUserID = userId;
                complaint.ComplaintStatusID = 1;
                await _db.Complaint.AddAsync(complaint);
                await _db.SaveChangesAsync();

                // adds message
                complaintUserMessage.ComplaintID = complaint.ComplaintID;
                complaintUserMessage.UserID = userId;
                complaintUserMessage.MessageDate = DateTime.Now;
                
                await _db.ComplaintUserMessage.AddAsync(complaintUserMessage);
                await _db.SaveChangesAsync();

                return RedirectToAction("Index");
            }
            else
            {
                _db.Complaint.Update(complaint);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
              
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Delete(Complaint complaint)
        {
            return RedirectToAction("Index");
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> addComplaintMessage(String Message, int ComplaintID, int ComplaintStatusID, int OldComplaintStatusID)
        {
            ComplaintUserMessage complaintUserMessage = new ComplaintUserMessage();
            complaintUserMessage.UserID = User.Identity.GetUserId(); ;
            complaintUserMessage.MessageDate = DateTime.Now;
            complaintUserMessage.ComplaintID = ComplaintID;
            complaintUserMessage.Message = Message;

            if (ComplaintStatusID != OldComplaintStatusID) {
                complaintUserMessage.Message += " The status was changed to " + _db.ComplaintStatus.Find(ComplaintStatusID).Description;
                Complaint complaint = _db.Complaint.Find(ComplaintID);

                complaint.ComplaintStatusID = ComplaintStatusID;
                _db.Complaint.Attach(complaint).Property(c => c.ComplaintStatusID).IsModified = true;
                await _db.SaveChangesAsync();

            }

            await _db.ComplaintUserMessage.AddAsync(complaintUserMessage);
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
