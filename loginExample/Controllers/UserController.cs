﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;

namespace loginExample.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly ApplicationDbContext _db;

      
        public UserController(ApplicationDbContext db, ILogger<UserController> logger)
        {
            _db = db;
            _logger = logger;
        }

        [Authorize(Roles = "admin, owner")]
        public  IActionResult Index(string searchFilter = null)
        {
            string userId = User.Identity.GetUserId();
            var users = new List<UserDetails>();

            if (User.IsInRole("admin"))
            {
                ViewBag.userRoles = _db.Roles.ToList();
                users = (from u in _db.Users
                             join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                             join r in _db.Roles on user_roles.RoleId equals r.Id
                             where (searchFilter != null ? u.UserName.Contains(searchFilter) : true) || (searchFilter != null ? u.PhoneNumber.Contains(searchFilter) : true) || (searchFilter != null ? r.Name.Contains(searchFilter) : true)
                             orderby u.UserName
                             select new UserDetails
                             {
                                 UserId = u.Id,
                                 UserName = u.UserName,
                                 Email = u.Email,
                                 PhoneNumber = u.PhoneNumber,
                                 RoleId = r.Id,
                                 RoleName = r.Name
                             }).ToList();

            }
            else {
                ViewBag.userRoles = _db.Roles.Where(r => r.Id.Equals("3") || r.Id.Equals("4"));

                users = (from u in _db.Users
                         join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                         join rental in _db.Rental on u.Id equals rental.UserID
                         join p in _db.Property on rental.PropertyID equals p.PropertyID
                         join r in _db.Roles on user_roles.RoleId equals r.Id
                         where p.UserId.Equals(userId) && (searchFilter != null ? u.UserName.Contains(searchFilter) : true) || (searchFilter != null ? u.PhoneNumber.Contains(searchFilter) : true) || (searchFilter != null ? r.Name.Contains(searchFilter) : true)
                         orderby u.UserName
                         select new UserDetails
                         {
                             UserId = u.Id,
                             UserName = u.UserName,
                             Email = u.Email,
                             PhoneNumber = u.PhoneNumber,
                             RoleId = r.Id,
                             RoleName = r.Name
                         }).ToList();

            }
            ViewBag.searchFilter = searchFilter;
          

            return View(users);
        }


        [HttpPost]
        public async Task<IActionResult> Upsert(UserDetails userDetails, IdentityUserRole role)
        {

            if (!userDetails.UserId.Equals("") || userDetails.UserId != null)
            {

                var roles = _db.UserRoles.Where(ur => ur.UserId == userDetails.UserId).ToList();
                _db.UserRoles.RemoveRange(roles);


                // lol worst code ever 
                var f = roles[0];
                f.RoleId = role.RoleId;

                var newRole = _db.UserRoles.Add(f);

               await _db.SaveChangesAsync();

            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin, owner")]
        public IActionResult Profile()
        {
            string userId = User.Identity.GetUserId();
            var UserDetails = _db.Users.Where(u => u.Id == userId).First();
            return View(UserDetails);
        }

        [HttpPost]
        public async Task<IActionResult> EditProfile(UserDetails userDetails, User user)
        {
            string userId = User.Identity.GetUserId();
            var userF = _db.Users.Where(u => u.Id == userId).First();

            var email = (String)userDetails.Email.Clone();

            userF.Email = userDetails.Email;
            userF.NormalizedEmail = email.ToUpper();
            userF.NormalizedUserName = userF.NormalizedEmail;
            userF.UserName = userDetails.UserName;
            userF.PhoneNumber = userDetails.PhoneNumber;

            _db.Users.Attach(userF).Property(u => u.Email).IsModified = true;
            _db.Users.Attach(userF).Property(u => u.NormalizedEmail).IsModified = true;
            _db.Users.Attach(userF).Property(u => u.NormalizedUserName).IsModified = true;
            _db.Users.Attach(userF).Property(u => u.UserName).IsModified = true;
            _db.Users.Attach(userF).Property(u => u.PhoneNumber).IsModified = true;

            _db.Users.Update(userF);

            await _db.SaveChangesAsync();
    
            return RedirectToAction("Profile");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
