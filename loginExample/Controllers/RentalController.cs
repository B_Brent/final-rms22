﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using System.Text;
using HtmlAgilityPack;

namespace loginExample.Controllers
{
    [Authorize]
    public class RentalController : Controller
    {
        private readonly ILogger<RentalController> _logger;
        private readonly ApplicationDbContext _db;

      
        public RentalController(ApplicationDbContext db, ILogger<RentalController> logger)
        {
            _db = db;
            _logger = logger;
        }
        public new Rental rental { get; set; }

       [AllowAnonymous]
        public IActionResult Index()
        {

            string userId = User.Identity.GetUserId();

            var rentals = new List<RentalDetails>();
            ViewBag.properties = new List<Property>();

            ViewBag.users = (from u in _db.Users
                            join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                            join r in _db.Roles on user_roles.RoleId equals r.Id
                            where r.Id != "4"
                            orderby u.UserName
                            select u
                            ).ToList();

            if (User.IsInRole("admin"))
            {
                ViewBag.properties = _db.Property.Where(p => p.active).ToList();
                rentals = ((from r in _db.Rental
                            join p in _db.Property on r.PropertyID equals p.PropertyID
                            join u in _db.Users on r.UserID equals u.Id
                            where r.Active
                            select new RentalDetails
                            {
                                RentalID = r.RentalID,
                                StartDate = r.StartDate,
                                EndDate = r.EndDate,
                                Deposit = r.Deposit,
                                MonthlyInstallment = r.MonthlyInstallment,
                                UserID = r.UserID,
                                PropertyID = r.PropertyID,
                                PropertyName = p.PropertyName,
                                UserName = u.UserName,
                                Active = r.Active
                            }).ToList());
            }
            else if (User.IsInRole("owner"))
            {

                ViewBag.properties = _db.Property.Where(p => p.active && p.UserId == userId).ToList();

                rentals = ((from r in _db.Rental
                            join p in _db.Property on r.PropertyID equals p.PropertyID
                            join u in _db.Users on r.UserID equals u.Id
                            where r.Active && p.UserId == userId
                            select new RentalDetails
                            {
                                RentalID = r.RentalID,
                                StartDate = r.StartDate,
                                EndDate = r.EndDate,
                                Deposit = r.Deposit,
                                MonthlyInstallment = r.MonthlyInstallment,
                                UserID = r.UserID,
                                PropertyID = r.PropertyID,
                                PropertyName = p.PropertyName,
                                UserName = u.UserName,
                                Active = r.Active
                            }).ToList());
            }

            else {
                rentals = ((from r in _db.Rental
                            join p in _db.Property on r.PropertyID equals p.PropertyID
                            join u in _db.Users on r.UserID equals u.Id
                            where r.Active && r.UserID == userId
                            select new RentalDetails
                            {
                                RentalID = r.RentalID,
                                StartDate = r.StartDate,
                                EndDate = r.EndDate,
                                Deposit = r.Deposit,
                                MonthlyInstallment = r.MonthlyInstallment,
                                UserID = r.UserID,
                                PropertyID = r.PropertyID,
                                PropertyName = p.PropertyName,
                                UserName = u.UserName,
                                Active = r.Active
                            }).ToList());
            }

            return View(rentals);

        }

        [Authorize(Roles = "admin, owner")]
        [HttpPost]
        public async Task<IActionResult> Upsert(RentalDetails rentalDetails, Rental rental, string removeListings) 
        { 

            if (rental.RentalID == 0 )
            {
                rental.Active = true;
                await _db.Rental.AddAsync(rental);
                await _db.SaveChangesAsync();
              
            }
            else
            {
                rental.Active = true;
                _db.Rental.Update(rental);
                await _db.SaveChangesAsync();
            }

            if (removeListings != null && removeListings.Equals("on")) {

                var listings = _db.Listing.Where(l => l.PropertyID == rental.PropertyID).ToList();

                foreach (Listing listing in listings)
                {
                    listing.ListingStatusID = 2;
                    _db.Listing.Attach(listing).Property(l => l.ListingStatusID).IsModified = true;
                    await _db.SaveChangesAsync();
                }  
            }

            return RedirectToAction("Index");

        }
        //Added This
        [HttpPost]
        public FileResult Export (string RentalHtml)
        {
            if (RentalHtml == null)
            {
                RentalHtml = "No Data Available to show";
            }
            HtmlNode.ElementsFlags["img"] = HtmlElementFlag.Closed;
            HtmlNode.ElementsFlags["input"] = HtmlElementFlag.Closed;
            HtmlDocument doc = new HtmlDocument();
            doc.OptionFixNestedTags = true;
            doc.LoadHtml(RentalHtml); 
            RentalHtml = doc.DocumentNode.OuterHtml;

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                Encoding unicode = Encoding.UTF8;
                StringReader sr = new StringReader(RentalHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "RentalDetails.pdf");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(RentalDetails RentalDetails, Rental rental)
        {
            rental.Active = false;
            _db.Rental.Attach(rental).Property(p => p.Active).IsModified = true;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
