﻿using loginExample.Data;
using loginExample.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;


namespace loginExample.Controllers
{
    
    [Authorize]
    public class NotificationController : Controller
    {
        private readonly ILogger<NotificationController> _logger;
        private readonly ApplicationDbContext _db;
        

        public NotificationController(ApplicationDbContext db, ILogger<NotificationController> logger)
        {
            _db = db;
            _logger = logger;
        }

        [AllowAnonymous]
        public IActionResult Index(string fromFilter = "default", int typeFilter = -1, string searchFilter = null)
        {
            string userId = User.Identity.GetUserId();
            ViewBag.fromFilter = fromFilter;
            ViewBag.typeFilter = typeFilter;
            ViewBag.searchFilter = searchFilter;
            ViewBag.notificationType = _db.NotificationType.ToList();
            ViewBag.users = (from u in _db.Users
                             join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                             join r in _db.Roles on user_roles.RoleId equals r.Id
                             where r.Id != "4"
                             orderby u.UserName
                             select u
                            ).ToList();

            List<MessageUser> messageUsers = new List<MessageUser>();
            messageUsers.Add(new MessageUser("all", "All Users"));
            messageUsers.Add(new MessageUser("admin,1", "All Admin"));
            messageUsers.Add(new MessageUser("owner,2", "All Owners"));
            messageUsers.Add(new MessageUser("tenant,3", "All Tenants"));

            var users = _db.Users.Where(u => u.Id != userId).ToList();
            foreach (IdentityUser u in users)
            {
                messageUsers.Add(new MessageUser(u.Id, u.UserName));
            }

            ViewBag.messageUsers = messageUsers;

            var notifications = (from a in _db.NotificationUser
                                 join n in _db.Notification on a.NotificationID equals n.NotificationID
                                 join u in _db.Users on n.UserID equals u.Id
                                 where a.UserID == userId && (fromFilter != "default" ? n.UserID == fromFilter : true) && (typeFilter > -1 ? n.TypeID == typeFilter : true) && (searchFilter != null ? n.Message.Contains(searchFilter) : true)
                                 orderby n.DateSent descending
                                 select new NotificationDetails
                                 {
                                     NotificationID = n.NotificationID,
                                     Message = n.Message,
                                     DateSent = n.DateSent,
                                     TypeID = n.TypeID,
                                     UserID = n.UserID,
                                     UserName = u.UserName
                                 }).ToList();


            return View(notifications);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Insert(Notification notification, NotificationDetails notificationDetails,  String messageUserID)
        {
            String usertID = User.Identity.GetUserId();

            List<String> userIds = new List<String>();

            if (messageUserID.Equals("all")) {
               userIds = (from u in _db.Users
                          join user_roles in _db.UserRoles on u.Id equals user_roles.UserId
                          join r in _db.Roles on user_roles.RoleId equals r.Id
                          where r.Id != "4"
                          orderby u.UserName
                          select u.Id
                          ).ToList();
            }
            else if (messageUserID.Equals("admin,1") || messageUserID.Equals("owner,2") || messageUserID.Equals("tenant,3"))
            {
                var roleID = messageUserID.Split(",")[1];
                userIds = (from r in _db.UserRoles
                           where r.RoleId == roleID && r.UserId != usertID
                           select r.UserId
                               ).ToList();
            }
            else {
                userIds.Add(messageUserID);
            }

            notification.DateSent = DateTime.Now;
            notification.UserID = usertID;

            await _db.Notification.AddAsync(notification);
            await _db.SaveChangesAsync();

            userIds.Add(notification.UserID);

            foreach (String UserId in userIds) {
                NotificationUser user = new NotificationUser(UserId, notification.NotificationID);
                await _db.NotificationUser.AddAsync(user);
            }

            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

    }
}
