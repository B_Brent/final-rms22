﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Schema;
using RMS22_Final_Project.Models;

namespace RMS22_Final_Project.Controllers
{
    public class RentalController : Controller
    {
        private readonly ApplicationDBContext _db;

        [BindProperty]
        public new Rental rental { get; set; }

        public RentalController(ApplicationDBContext db) {
            _db = db;
        }

        public  IActionResult Index()
        {
            rental = new Rental();
            rental.RentalID = 0;

            ViewBag.users =  _db.Users.ToList();
            ViewBag.properties = _db.Property.Where(p => p.active).ToList();

            return View(rental);
        }

        #region Api Calls   

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var rentals = await (from r in _db.Rental
                join p in _db.Property on r.PropertyID equals p.PropertyID
                join u in _db.Users on r.UserID equals u.UserID
                where r.Active
                select new
                { 
                    r.RentalID,
                    r.StartDate,
                    r.EndDate, 
                    r.Deposit,
                    r.MonthlyInstallment,
                    r.UserID,
                    r.PropertyID,
                    p.PropertyName,
                    u.UserName,
                    r.Active

                }).ToListAsync();

            return Json(new { data = rentals });
        }

       
        [HttpPost]
        public async Task<IActionResult> Delete(Rental rental)
        {
            var rentalFromDB = await _db.Rental.FirstAsync(r => r.RentalID == rental.RentalID);
            rentalFromDB.Active = false;
            _db.Rental.Attach(rentalFromDB).Property(r => r.Active).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        [HttpPost]
        public async Task<IActionResult> Upsert(Rental rental)
        {
            if (rental.RentalID == 0)
            {
                // todo check if logic no rental in same time span 
                var rentalCount = await _db.Rental.CountAsync(r => r.EndDate > rental.StartDate && r.Active && r.PropertyID == rental.PropertyID);

                if (rentalCount == 0)
                {
                    rental.Active = true; 
                    await _db.Rental.AddAsync(rental);
                    await _db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else {
                    return RedirectToAction("Index");
                }

            }
            else {
                rental.Active = true;
                _db.Rental.Update(rental);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
        }

        #endregion
    }
}
