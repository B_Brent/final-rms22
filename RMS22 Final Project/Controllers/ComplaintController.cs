﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Schema;
using RMS22_Final_Project.Models;

namespace RMS22_Final_Project.Controllers
{
    public class ComplaintController : Controller
    {
        private readonly ApplicationDBContext _db;
        public ComplaintController(ApplicationDBContext db) {
            _db = db;
        }

        public  IActionResult Index()
        {
            ViewBag.users =  _db.Users.ToList();
            ViewBag.status = _db.ComplaintStatus.ToList();
            ViewBag.types = _db.ComplaintType.ToList();


            var userID = 16;

            // SELECT c.ComplaintID, c.ReporterUserID, c.Title, c.ComplaintStatusID, c.RecieverUserID, c.ComplaintTypeID, c.ComplaintStatusID, t.Description as "typeDesc", s.Description as "statusDesc", m.ComplaintUserMessageID, m.UserID, m.MessageDate, m.Message
            // FROM Complaint c
            // JOIN ComplaintType t on c.ComplaintTypeID = t.TypeID
            // JOIN ComplaintStatus s on c.ComplaintStatusID = s.StatusID
            // LEFT JOIN ComplaintUserMessage m on c.ComplaintID = m.ComplaintID
            // WHERE c.ReporterUserID = 16 OR c.RecieverUserID = 16

            // m needs to be a left join

            var complaints = (from c in _db.Complaint
                              join t in _db.ComplaintType on c.ComplaintTypeID equals t.TypeID
                              join s in _db.ComplaintStatus on c.ComplaintStatusID equals s.StatusID
                              join m in _db.ComplaintUserMessage on c.ComplaintID equals m.ComplaintUserMessageID
                              where ( c.RecieverUserID == userID || c.ReporterUserID == userID )
                              select new
                              {
                                  c.ComplaintID,
                                  c.ReporterUserID,
                                  c.Title,
                                  c.ComplaintStatusID,
                                  c.RecieverUserID,
                                  c.ComplaintTypeID,
                                  typeDesc = t.Description,
                                  statusDesc = s.Description,
                                  m.ComplaintUserMessageID,
                                  m.UserID,
                                  m.MessageDate,
                                  m.Message

                              }).ToListAsync();


            return View();
        }

        #region Api Calls   

    
        //[HttpPost]
        //public async Task<IActionResult> Delete(Rental rental)
        //{
        //    var rentalFromDB = await _db.Rental.FirstAsync(r => r.RentalID == rental.RentalID);
        //    rentalFromDB.Active = false;
        //    _db.Rental.Attach(rentalFromDB).Property(r => r.Active).IsModified = true;
        //    await _db.SaveChangesAsync();

        //    return RedirectToAction("Index");
        //}


        //[HttpPost]
        //public async Task<IActionResult> Upsert(Rental rental)
        //{
        //    if (rental.RentalID == 0)
        //    {
        //        // todo check if logic no rental in same time span 
        //        var rentalCount = await _db.Rental.CountAsync(r => r.EndDate > rental.StartDate && r.Active && r.PropertyID == rental.PropertyID);

        //        if (rentalCount == 0)
        //        {
        //            rental.Active = true; 
        //            await _db.Rental.AddAsync(rental);
        //            await _db.SaveChangesAsync();
        //            return RedirectToAction("Index");
        //        }
        //        else {
        //            return RedirectToAction("Index");
        //        }

        //    }
        //    else {
        //        rental.Active = true;
        //        _db.Rental.Update(rental);
        //        await _db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //}

        #endregion
    }
}
