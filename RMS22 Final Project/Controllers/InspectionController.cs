﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Schema;
using RMS22_Final_Project.Models;

namespace RMS22_Final_Project.Controllers
{
    public class InspectionController : Controller
    {
        private readonly ApplicationDBContext _db;
       
        [BindProperty]
        public new Inspection inspection { get; set; }
        [BindProperty]
        public new InspectionItem inspectionItem { get; set; }

        public InspectionController(ApplicationDBContext db) {
            _db = db;
        }

        public  IActionResult Index()
        {
            inspection = new Inspection();
            inspection.InspectionID = 0;

            ViewBag.properties = _db.Property.Where(p => p.active).ToList();
          
            return View(inspection);
        }

        
        public IActionResult Items(int id)
        {
            inspectionItem = new InspectionItem();
            inspectionItem.ItemID = 0;

            ViewBag.result = _db.InspectionResult.ToList();
            ViewBag.inspectionID = id;

            return View("Items", inspectionItem);
        }
        #region Api Calls   

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var inspections = await (from i in _db.Inspection
                join p in _db.Property on i.PropertyID equals p.PropertyID
                where i.Active
                select new
                { 
                    i.Active,
                    i.Details,
                    i.InspectionDate,
                    i.InspectionID,
                    i.InspectionStatus,
                    i.PropertyID,
                    p.PropertyName

                }).ToListAsync();

            return Json(new { data = inspections });
        }

       
        [HttpPost]
        public async Task<IActionResult> Delete(Inspection inspection)
        {
            var inspectionFromDB = await _db.Inspection.FirstAsync(i => i.InspectionID == inspection.InspectionID);
            inspectionFromDB.Active = false;
            _db.Inspection.Attach(inspectionFromDB).Property(i => i.Active).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        [HttpPost]
        public async Task<IActionResult> Upsert(Inspection inspection)
        {
            if (inspection.InspectionID == 0)
            {
                inspection.Active = true;
                await _db.Inspection.AddAsync(inspection);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            else {
                inspection.Active = true;
                _db.Inspection.Update(inspection);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> getItems(int inspectionId)
        {
            // todo fix passing the id 
            inspectionId = 1; 
            var items  = await (from i in _db.InspectionItem
                                     join r in _db.InspectionResult on i.ResultID equals r.ResultID
                                     where (i.Active && i.InspectionID == inspectionId)
                                     select new 
                                     {
                                         i.ItemID,
                                         i.Active,
                                         i.InspectionID,
                                         i.ResultID,
                                         i.Name,
                                         i.Notes,
                                         i.Description,
                                         resultDescription = r.Description 
                                     }).ToListAsync();

            return Json(new { data = items });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteItem(InspectionItem inspectionItem)
        {
            var inspectionItemFromDB = await _db.InspectionItem.FirstAsync(i => i.ItemID == inspectionItem.ItemID);
            inspectionItemFromDB.Active = false;
            _db.InspectionItem.Attach(inspectionItemFromDB).Property(i => i.Active).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Items");
        }


        [HttpPost]
        public async Task<IActionResult> UpsertItem(InspectionItem inspectionItem)
        {
            if (inspectionItem.ItemID == 0)
            {
                inspectionItem.Active = true;
                await _db.InspectionItem.AddAsync(inspectionItem);
                await _db.SaveChangesAsync();
                return RedirectToAction("Items");

            }
            else
            {
                inspectionItem.Active = true;
                _db.InspectionItem.Update(inspectionItem);
                await _db.SaveChangesAsync();
                return RedirectToAction("Items");
            }
        }

        #endregion
    }
}
