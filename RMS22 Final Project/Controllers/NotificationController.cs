﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Schema;
using RMS22_Final_Project.Models;

namespace RMS22_Final_Project.Controllers
{
    public class NotificationController : Controller
    {
        private readonly ApplicationDBContext _db;

        [BindProperty]
        public new Notification notification { get; set; }

        public NotificationController(ApplicationDBContext db) {
            _db = db;
        }

        public  IActionResult Index()
        {
            notification = new Notification();
            notification.NotificationID = 0;

            ViewBag.notificationType =  _db.NotificationType.ToList();
            ViewBag.users = _db.Users.ToList();


            var users = _db.Users.ToList();
          
            ArrayList messageUsers = new ArrayList();
            MessageUser all = new MessageUser(-1, "All Users");
            MessageUser admin = new MessageUser(-1, "All Admin");
            MessageUser owner = new MessageUser(-2, "All Owners");
            MessageUser tenant = new MessageUser(-3, "All Tenants");

            messageUsers.Add(all);
            messageUsers.Add(admin);
            messageUsers.Add(owner);
            messageUsers.Add(tenant);

            foreach (User u in users) {
                 messageUsers.Add(new MessageUser(u.UserID, u.UserName));
            }

            ViewBag.messageUsers = messageUsers;


            // copied code
            var userId = 16;
            ViewBag.Notifications = (from n in _db.Notification
                                      join t in _db.NotificationType on n.TypeID equals t.TypeID
                                      join u in _db.Users on n.UserID equals u.UserID
                                      join nu in _db.NotificationUser on n.NotificationID equals nu.NotificationID
                                      where u.UserID == userId
                                      select new NotificationMessages
                                      {
                                          Message = n.Message,
                                          DateSent = n.DateSent,
                                          TypeID = n.TypeID,
                                          UserName = u.UserName,
                                          DescriptionType = t.DescriptionType
                                      }).ToList();


            return View(notification);
        }

        #region Api Calls   

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {

            // todo get only the notifications for the logged in user
            var userId = 16;
            var notifications = await (from n in _db.Notification
                                       join t in _db.NotificationType on n.TypeID equals t.TypeID
                                       join u in _db.Users on n.UserID equals u.UserID
                                       join nu in _db.NotificationUser on n.NotificationID equals nu.NotificationID  
                                       where  u.UserID == userId
                                       select new { 
                                        n.NotificationID,
                                        n.Message,
                                        n.DateSent,
                                        n.TypeID,
                                        n.UserID,
                                        u.UserName,
                                        u.FirstName,
                                        u.LastName,
                                        t.DescriptionType

                                    }).ToListAsync();

            return Json(new { data = notifications });
        }

        [HttpPost]
        public async Task<IActionResult> Insert(Notification notification, String addtional)
        {
            notification.DateSent = DateTime.Now;
            notification.UserID = 16;

            await _db.Notification.AddAsync(notification);
            await _db.SaveChangesAsync();

            var a = ViewData["userTo"];

            // insert to all the other 
            NotificationUser user = new NotificationUser(notification.UserID, notification.NotificationID);
            await _db.NotificationUser.AddAsync(user);
            await _db.SaveChangesAsync();

            // Add all other users 

            return RedirectToAction("Index");
        }

        #endregion
    }
}
