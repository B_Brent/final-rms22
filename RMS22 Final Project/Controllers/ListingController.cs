﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RMS22_Final_Project.Models;

namespace RMS22_Final_Project.Controllers
{
    public class ListingController : Controller
    {
        public new Listing Listing { get; set; }

        private readonly ApplicationDBContext _db;

        public ListingController(ApplicationDBContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {

            Listing = new Listing();
            Listing.ListID = 0;

            ////todo get users username, id only
            ViewBag.propertyID = _db.Property.ToList();
            ViewBag.listingStatusDescription = _db.ListingStatus.ToList();

            return View(Listing);
        }

        #region Api Calls

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var listings = await (from l in _db.Listing
                                  join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                                  join p in _db.Property on l.PropertyID equals p.PropertyID
                                  orderby l.AvailabilityDate
                                  where l.ListingStatusID != 3
                                  select new
                                  {
                                      l.ListID,
                                      l.ListingPrice,
                                      l.AvailabilityDate,
                                      l.PropertyID,
                                      l.ListingStatusID,
                                      ls.ListingStatusDescription,
                                      p.PropertyName
                                  }).ToListAsync();

            return Json(new { data = listings });
        }

        [HttpPost]

        public async Task<IActionResult> Delete(Listing listing)
        {
            listing.ListingStatusID = 3;
            _db.Listing.Attach(listing).Property(l => l.ListingStatusID).IsModified = true;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index");

        }

        [HttpPost]
        public async Task<IActionResult> Upsert(Listing listing)
        {
            if (listing.ListID == 0)
            {

                await _db.Listing.AddAsync(listing);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

                //var listingCount = await _db.Listing.CountAsync(l => l.ListID.Equals(listing.ListID));

                //if (listingCount == 0)
                //{

                //}
                //else
                //{
                //    // ViewBag.Message = "Property listing already exist";
                //    return RedirectToAction("Index");
                //    //return Json(new { messsage = "Property listing already exist" });
                //}

            }
            else
            {
                _db.Listing.Update(listing);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
                //  ViewBag.Message = "Updated property details successfully";
            }
        }

        #endregion
    }
}
