﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RMS22_Final_Project.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace RMS22_Final_Project.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly ApplicationDBContext _db;

        public HomeController(ApplicationDBContext db)
        {
            _db = db;
           
        }

        public ActionResult Index()
        {
            var listings = (from l in _db.Listing
                            join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                            join p in _db.Property on l.PropertyID equals p.PropertyID
                            join u in _db.Users on p.UserID equals u.UserID
                            join s in _db.Suburb on p.SuburbID equals s.SuburbID
                            orderby l.AvailabilityDate
                            where (l.ListingStatusID == 1 && p.active )  
                            select new AvailableListing
                            {
                                PropertyID = p.PropertyID,
                                ListingPrice = l.ListingPrice,
                                AvailabilityDate = l.AvailabilityDate,
                                ListingStatusDescription = ls.ListingStatusDescription,
                                PropertyName = p.PropertyName,
                                PropertyAddress = p.PropertyAddress,
                                RoomNum = p.RoomNum,
                                PropertyDescription = p.PropertyDescription,
                                SuburbName = s.SuburbName,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                CellNo = u.CellNo,
                                Email = u.Email,
                                size = p.size,
                                ParkNum = p.ParkNum,
                                AllowPets = p.AllowPets,
                                BathNum = p.BathNum
                            }).ToList();


            ViewBag.listings = listings;

            ViewBag.suburbs = _db.Suburb.ToList(); 

            return View();
        }


        [HttpGet]
        public async Task<IActionResult> GetAvailableListing()
        {
            var listings = await (from l in _db.Listing
                                  join ls in _db.ListingStatus on l.ListingStatusID equals ls.ListingStatusID
                                  join p in _db.Property on l.PropertyID equals p.PropertyID
                                  join u in _db.Users on p.UserID equals u.UserID
                                  join s in _db.Suburb on p.SuburbID equals s.SuburbID
                                  orderby l.AvailabilityDate
                                  where l.ListingStatusID == 1
                                  select new
                                  {
                                      l.ListID,
                                      l.ListingPrice,
                                      l.AvailabilityDate,
                                      l.PropertyID,
                                      ls.ListingStatusDescription,
                                      p.PropertyName,
                                      p.PropertyAddress,
                                      p.RoomNum,
                                      s.SuburbName,
                                      u.FirstName,
                                      u.LastName,
                                      u.CellNo,
                                      u.Email
                                  }).ToListAsync();

            return Json(new { data = listings });
        }

        public IActionResult Privacy()
        {
            return View();
        }

    }
}
