﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Schema;
using RMS22_Final_Project.Models;

namespace RMS22_Final_Project.Controllers
{
    public class UsersController : Controller
    {
        private readonly ApplicationDBContext _db;

        [BindProperty]
        public new User User { get; set; }

        public UsersController(ApplicationDBContext db) {
            _db = db;
        }

        public  IActionResult Index()
        {
            User = new User();
            User.UserID = 0;

            ViewBag.userRoles =  _db.UserRoles.ToList();

            return View(User);
        }

        #region Api Calls   

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var users = await (from u in _db.Users
                join ur in _db.UserRoles on u.UserRoleID equals ur.RoleID
                where u.active.Equals(true)
                orderby u.UserName
                select new
                { 
                    u.UserName,
                    u.FirstName,
                    u.CellNo,
                    u.Email,
                    u.LastName,
                    u.UserID,
                    u.UserRoleID,
                    ur.description

                }).ToListAsync();

            return Json(new { data = users });
        }

        [HttpGet]
        public async Task<IActionResult> GetUserRoles()
        {
            return Json(new { data = await _db.UserRoles.ToListAsync() });
        }

        [HttpPost]
        public async Task<IActionResult> Delete(User user)
        {
            var UserFromDB = await _db.Users.FirstAsync(u => u.UserID == user.UserID);
            if (UserFromDB == null)
            {
               // ViewBag.Message = "Error whlie Deleting";
            }
            else {
            
                UserFromDB.active = false; 
                _db.Users.Attach(UserFromDB).Property(u => u.active).IsModified = true;
                await _db.SaveChangesAsync();
               // ViewBag.Message = "Delete Successful";
            }

            return RedirectToAction("Index");

        }


        [HttpPost]
        public async Task<IActionResult> Upsert(User user)
        {
            if (user.UserID == 0)
            {
                // todo check if user name does not already exist;
                var userCount = await _db.Users.CountAsync(u => u.UserName.Equals(user.UserName));

                if (userCount == 0)
                {
                    user.active = true; 
                    await _db.Users.AddAsync(user);
                    await _db.SaveChangesAsync();
                    // ViewBag.Message = "Added New User Successful";
                    return RedirectToAction("Index");
                }
                else {
                    // ViewBag.Message = "User name already exist";
                    return RedirectToAction("Index");
                    //return Json(new { messsage = "User name already exist" });
                }

            }
            else {
                user.active = true;
                _db.Users.Update(user);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
                //  ViewBag.Message = "Updated User Details Successful";
            }
        }

        #endregion
    }
}
