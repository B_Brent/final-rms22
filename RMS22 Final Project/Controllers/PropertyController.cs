﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RMS22_Final_Project.Models;
using System.Web;
using System.IO;

namespace RMS22_Final_Project.Controllers
{
    public class PropertyController : Controller
    {

        [BindProperty]
        public new Property Property { get; set; }

        private readonly ApplicationDBContext _db;

     
        public PropertyController(ApplicationDBContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            Property = new Property();
            Property.PropertyID = 0;

            //todo get users username, id only
            ViewBag.owners = _db.Users.ToList();
            ViewBag.suburbs = _db.Suburb.ToList();

            return View(Property);
        }

        #region Api Calls   

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var properties = await (from p in _db.Property
                               join s in _db.Suburb on p.SuburbID equals s.SuburbID
                               join u in _db.Users on p.UserID equals u.UserID
                               where p.active == true
                               orderby p.PropertyName
                               select new
                               {
                                   u.UserName,
                                   p.PropertyName,
                                   p.PropertyID,
                                   p.PropertyAddress,
                                   p.PropertyDescription,
                                   p.RoomNum,
                                   p.SuburbID,
                                   p.UserID,
                                   p.active,
                                   p.AllowPets,
                                   p.BathNum,
                                   p.size,
                                   p.ParkNum,
                                   s.SuburbName

                               }).ToListAsync();

            return Json(new { data = properties });
        }


        [HttpPost]
        public async Task<IActionResult> Delete(Property property)
        {
            property.active = false;
            _db.Property.Attach(property).Property(p => p.active).IsModified = true;
            await _db.SaveChangesAsync();
           
            return RedirectToAction("Index");

        }


        [HttpPost]
        public async Task<IActionResult> Upsert(Property property)
        {
            if (property.PropertyID == 0)
            {

                var propertyCount = await _db.Property.CountAsync(p => p.PropertyName.Equals(property.PropertyName));

                if (propertyCount == 0)
                {
                    property.active = true;
                    await _db.Property.AddAsync(property);
                    await _db.SaveChangesAsync();
                    // ViewBag.Message = "Added New User Successfully";
                    return RedirectToAction("Index");
                }
                else {
                    return RedirectToAction("Index");
                    // name already exists
                }
            }
            else
            {
                property.active = true;
                _db.Property.Update(property);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
                //  ViewBag.Message = "Updated User Details Successfully";
            }
        }

        [HttpPost]
        public IActionResult UploadImage(HttpPostAttribute file)
        {
            
               //fix using HttpPostedFilebased ?  
           // int d = 5;

            return RedirectToAction("Index");

        }


        #endregion

    }
}
