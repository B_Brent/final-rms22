﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class MessageUser
    {
        public MessageUser(int userID, string userName)
        {
            UserID = userID;
            UserName = userName;
        }

        public int UserID { get; set;}

        public string UserName { get; set; }
      
    }
}
