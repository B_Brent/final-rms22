﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class AvailableListing
    {
       public int PropertyID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CellNo { get; set; }

        public string Email { get; set; }

        public double ListingPrice { get; set; }

        public DateTime AvailabilityDate { get; set; }

        public string PropertyName { get; set; }

        public string PropertyDescription { get; set; }

        public int RoomNum { get; set; }

        public string PropertyAddress { get; set; }

        public string SuburbName { get; set; }

        public string ListingStatusDescription { get; set; }

        public int size { get; set; }
        public int ParkNum { get; set; }
        public int BathNum { get; set; }

        public bool AllowPets { get; set; }

    }
}
