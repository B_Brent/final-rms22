﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class Listing
    {
        [Key]
        public int ListID { get; set; }
        public int ListingStatusID { get; set; }
        public double ListingPrice { get; set; }

        public DateTime AvailabilityDate { get; set; }
        public int PropertyID { get; set; }

    }
}
