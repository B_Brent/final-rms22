﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class UserRoles
    {
        [Key]
        public int RoleID { get; set; }
        public string description { get; set; }
        
    }
}
