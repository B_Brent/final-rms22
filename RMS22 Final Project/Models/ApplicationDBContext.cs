﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class ApplicationDBContext : DbContext
    {

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<Suburb> Suburb { get; set; }
        public DbSet<Property> Property { get; set; }
        public DbSet<Listing> Listing { get; set; }
        public DbSet<ListingStatus> ListingStatus { get; set; }
        public DbSet<Rental> Rental { get; set; }
        public DbSet<Inspection> Inspection { get; set; }
        public DbSet<InspectionItem> InspectionItem { get; set; }
        public DbSet<InspectionResult> InspectionResult { get; set; }

        public DbSet<Notification> Notification { get; set; }
        public DbSet<NotificationType> NotificationType { get; set; }
        public DbSet<NotificationUser> NotificationUser { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NotificationUser>()
                .HasKey(nu => new { nu.UserID, nu.NotificationID });
        }

        public DbSet<Complaint> Complaint { get; set; }

        public DbSet<ComplaintStatus> ComplaintStatus { get; set; }
       
        public DbSet<ComplaintType> ComplaintType { get; set; }

        public DbSet<ComplaintUserMessage> ComplaintUserMessage { get; set; }
    }
}
 