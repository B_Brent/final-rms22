﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class InspectionItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemID { get; set; }
        public int InspectionID { get; set; }
        public int ResultID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

    }
}
