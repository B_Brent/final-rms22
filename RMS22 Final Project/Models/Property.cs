﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class Property
    {
        [Key]
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public int UserID { get; set; }
        public int SuburbID { get; set; }       
        public string PropertyDescription { get; set; }
        public int RoomNum { get; set; }
        public string PropertyAddress{ get; set; }          
        
        public bool active { get; set; }

        public int size { get; set; }
        public int ParkNum { get; set; }
        public int BathNum { get; set; }

        public bool AllowPets { get; set; }
        
    }
}
