﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class Complaint
    {
        [Key]
        public int ComplaintID { get; set; }
        public int RecieverUserID { get; set; }
        public int ReporterUserID { get; set; }
        public string Title { get; set; }
        public int ComplaintTypeID { get; set; }
        public int ComplaintStatusID { get; set; }

    }
}
