﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class NotificationUser
    {
        public NotificationUser(int userID, int notificationID)
        {
            UserID = userID;
            NotificationID = notificationID;
        }

        [Key]
        public int UserID { get; set; }
        [Key]
        public int NotificationID { get; set; }
    }
}
