﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class Rental
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RentalID { get; set;}

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double MonthlyInstallment { get; set; }
        public double Deposit { get; set; }
        public int PropertyID { get; set; }
        public int UserID { get; set; }
        public bool Active { get; set; }
       
    }
}
