﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMS22_Final_Project.Models
{
    public class ComplaintUserMessage
    {
        [Key]
        public int ComplaintUserMessageID { get; set; }
        public int UserID { get; set; }
        public int ComplaintID { get; set; }
        public DateTime MessageDate { get; set; }
        public int Message { get; set; }

    }
}
