﻿var dataTable;
var inspectionItemData = {};

$(document).ready(function () {
    loadData().then(function () {
        loadDataTable();
    });
});


function loadData() {
    var getInspectionItems = new Promise((resolve, reject) => {
        var id = window.location.pathname.slice(18, window.location.pathname.length);
        $.ajax({
            url: '/inspection/getItems/' + id,
            type: 'GET',
            data: { id: id},
            dataType: 'json',
            success: function (data) {
                inspectionItemData.data = data.data;
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    });

    return Promise.all([getInspectionItems]);
}

function loadDataTable() {
    dataTable = $('#DT_load').dataTable({
        "data": inspectionItemData.data,
        "scrollX": true,
        "columns": [  
            { "data": "name" },
            { "data": "description" },
            { "data": "resultDescription" },
            { "data": "notes" },
            {
                "data": "itemID",
                "render": function (data) {

                    return `<div class="text-center">
                                <a class='btn btn-success text-white btn-sm' onclick=Upsert(${data})> Edit </a>
                                <a class='btn btn-danger text-white btn-sm' onclick=Delete(${data})> Delete </a>
                            </div>`
                },
                "width": "175px"
            }
        ],
        "lanquage": {
            "emptyTable": "No Data"
        },
        "width": "100%"
    })
}

function Upsert(id) {
    var inspectionItem = inspectionItemData.data.find(function (value) {
        return value.itemID == id
    }) || {
        itemID: undefined,
        active: true,
        inspectionID: window.location.pathname.slice(18, window.location.pathname.length), 
        resultID: '',
        name: '',
        notes: '',
        description: '',
        resultDescription: ''
    }; 

    $('#txtItemID').val(inspectionItem.itemID);
    $('#txtInspectionID').val(inspectionItem.inspectionID);
    $('#ddlResultID').val(inspectionItem.resultID);
    $('#txtName').val(inspectionItem.name);
    $('#txtNotes').val(inspectionItem.notes);
    $('#txtDescription').val(inspectionItem.description);
      
    $('#upsertInspectionItem').modal(open);
}

function Delete(id) {
    $('#txtDeleteInspectionItemID').val(id);
    $('#deleteInspectionItem').modal(open);
}
