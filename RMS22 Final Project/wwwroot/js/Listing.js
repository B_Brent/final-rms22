﻿var dataTable;
var propertyListingData = {};

$(document).ready(function () {
    loadData().then(function () {
        loadDataTable();
    });
});

function loadData() {
    var getListing = new Promise((resolve, reject) => {
        $.ajax({
            url: '/listing/getall',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                propertyListingData.data = data.data;
                resolve(data);
            },
            error: function (error) {
                reject(error);
            },
        });
    });
    return Promise.all([getListing]);
}

function loadDataTable() {
        dataTable = $('#DT_load').dataTable({
            "data": propertyListingData.data,
            "columns": [
                { "data": "propertyName" },
                { "data": "listingPrice" },
                { "data": "availabilityDate" },
                { "data": "listingStatusDescription" },
                {
                    "data": "listID",
                    "render": function (data) {

                        return `<div class="text-center">
                                <a class='btn btn-success text-white btn-sm' onclick=Upsert(${data})> Edit </a>
                                <a class='btn btn-danger text-white btn-sm' onclick=Delete(${data})> Delete </a>
                            </div>`
                    }
                }
            ],
            "lanquage": {
                "emptyTable": "No Data"
            },
            "width": "100%"
        })
    }

    function Upsert(id) {
        var listing = propertyListingData.data.find(function (value) {
            return value.listID == id
        }) || {
            listID: undefined,
            listingPrice: '',
            availabilityDate: '',
            propertyID: '',
            listingStatusID: '',
            listStatusDescription: '',
            propertyName: ''
        };

        $('#txtListID').val(listing.listID);
        $('#ddlPropertyID').val(listing.propertyID);
        $('#txtListingPrice').val(listing.listingPrice);
        $('#txtAvailabilityDate').val(listing.availabilityDate);
        $('#ddlListStatusID').val(listing.listingStatusID);
    

        $('#upsertListing').modal(open);
    }

    function Delete(id) {
        $('#txtDeletePropertyID').val(id);
        $('#deleteListing').modal(open);
    }
