﻿var dataTable;
var propertyData = {};

$(document).ready(function () {
    loadData().then(function () {
        loadDataTable();
    });
});


function loadData() {
    var getUsers = new Promise((resolve, reject) => {
        $.ajax({
            url: '/property/getall',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                propertyData.data = data.data;
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    });

    return Promise.all([getUsers]);
}

function loadDataTable() {
    dataTable = $('#DT_load').dataTable({
        "data": propertyData.data,
        "scrollX": true,
        "columns": [    
            { "data": "propertyName" },
            {
                "data": "roomNum",
                "width": "15px"
            },
            { "data": "bathNum" },
            { "data": "parkNum" },
            { "data": "size" },
            { "data": "allowPets" },
            { "data": "propertyAddress" },   
            { "data": "suburbName" },
            { "data": "userName" },
            { "data": "propertyDescription" },
            {
                "data": "propertyID",
                "render": function (data) {

                    return `<div class="text-center">
                                <a class='btn btn-info text-white btn-sm' onclick=showImages(${data})> Images </a>
                                <a class='btn btn-success text-white btn-sm' onclick=Upsert(${data})> Edit </a>
                                <a class='btn btn-danger text-white btn-sm' onclick=Delete(${data})> Delete </a>
                            </div>`
                },
                "width": "20%"
            }
        ],
        "lanquage": {
            "emptyTable": "No Data"
        },
        "width": "100%"
    })
}

function Upsert(id) {
    var property = propertyData.data.find(function (value) {
        return value.propertyID == id
    }) || {
        propertyID: undefined,
        propertyName: '',
        propertyDescription: '', 
        roomNum: 0,
        bathNum: 0,
        parkNum: 0,
        size: 0,
        allowPets: false,
        propertyAddress: '',
        suburbID: '',
        userID: ''
    }; 

    $('#txtPropertyName').val(property.propertyName);
    $('#txtPropertyID').val(property.propertyID);
    $('#txtPropertyDescription').val(property.propertyDescription);
    $('#txtRoomNum').val(property.roomNum);

    $('#txtBathNum').val(property.bathNum);
    $('#txtParkNum').val(property.parkNum);
    $('#txtSize').val(property.size);
    $('#txtAllowPets').val(property.allowPets);

    $('#txtPropertyAddress').val(property.propertyAddress);

    $('#ddlSuburbID').val(property.suburbID);
    $('#ddlUserID').val(property.userID);     

    $('#upsertProperty').modal(open);
}

function Delete(id) {
    $('#txtDeletePropertyID').val(id);
    $('#deleteProperty').modal(open);
}

function showImages(id) {
    $('#txtImagePropertyID').val(id);
    $('#propertyImages').modal(open);
}