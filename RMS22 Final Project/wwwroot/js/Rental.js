﻿var dataTable;
var rentalData = {};

$(document).ready(function () {
    loadData().then(function () {
        loadDataTable();
    });
});


function loadData() {
    var getRentals = new Promise((resolve, reject) => {
        $.ajax({
            url: '/rental/getall',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                rentalData.data = data.data;
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    });

    return Promise.all([getRentals]);
}

function loadDataTable() {
    dataTable = $('#DT_load').dataTable({
        "data": rentalData.data,
        "scrollX": true,
        "columns": [  
            { "data": "propertyName" },
            { "data": "startDate" },
            { "data": "endDate" },
            { "data": "deposit" },
            { "data": "monthlyInstallment" },
            { "data": "userName" },   
        
            {
                "data": "rentalID",
                "render": function (data) {

                    return `<div class="text-center">
                                <a class='btn btn-success text-white btn-sm' onclick=Upsert(${data})> Edit </a>
                                <a class='btn btn-danger text-white btn-sm' onclick=Delete(${data})> Delete </a>
                            </div>`
                },
                "width": "175px"
            }
        ],
        "lanquage": {
            "emptyTable": "No Data"
        },
        "width": "100%"
    })
}

function Upsert(id) {
    var rental = rentalData.data.find(function (value) {
        return value.rentalID == id
    }) || {
        rentalID: undefined,
        startDate: '',
        endDate: '', 
        deposit: 0,
        monthlyInstallment: 0,
        userID: 0,
        propertyID: 0,
        active: true
    }; 

    var s = (rental.startDate).slice(0, 10);

    $('#txtRentalID').val(rental.rentalID);
    $('#txtStartDate').val((rental.startDate).slice(0, 10));
    $('#txtEndDate').val((rental.endDate).slice(0, 10));
    $('#txtDeposit').val(rental.deposit);
    $('#txtMonthlyInstallment').val(rental.monthlyInstallment);
    $('#ddlUserID').val(rental.userID);
    $('#ddlPropertyID').val(rental.propertyID);
      

    $('#upsertRental').modal(open);
}

function Delete(id) {
    $('#txtDeleteRentalID').val(id);
    $('#deleteRental').modal(open);
}