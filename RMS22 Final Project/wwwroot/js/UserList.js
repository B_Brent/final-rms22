﻿var dataTable;
var userData = {};

$(document).ready(function () {
    loadData().then(function () {
        loadDataTable();
    });
});


function loadData() {
    var getUsers = new Promise((resolve, reject) => {
        $.ajax({
            url: '/users/getall',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                userData.data = data.data;
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    });

    var getRoles = new Promise((resolve, reject) => {
        $.ajax({
            url: '/users/GetUserRoles',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                userData.userRoles = data.data;
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    });

    return Promise.all([getUsers, getRoles]);
}

function loadDataTable() {
    dataTable = $('#DT_load').dataTable({
        "data": userData.data,
        "columns": [
            { "data": "userName" },
            { "data": "firstName" },
            { "data": "lastName" },
            { "data": "cellNo" },
            { "data": "email" },
            { "data": "description" },
            {
                "data": "userID",
                "render": function (data) {

                    return `<div class="text-center">
                                <a class='btn btn-success text-white btn-sm' onclick=Upsert(${data})> Edit </a>
                                <a class='btn btn-danger text-white btn-sm' onclick=Delete(${data})> Delete </a>
                            </div>`
                }
            }
        ],
        "lanquage": {
            "emptyTable": "No Data"
        },
        "width": "100%"
    })
}

function Upsert(id) {
    var user = userData.data.find(function (value) {
        return value.userID == id
    }) || {
        userID: undefined,
        firstName: '', 
        lastName: '',
        cellNo: '',
        email: ''
    }; 

    $('#txtUserID').val(user.userID);
    $('#txtUserName').val(user.userName);
    $('#txtFirstName').val(user.firstName);
    $('#txtLastName').val(user.lastName);
    $('#txtCellNo').val(user.cellNo);
    $('#txtEmail').val(user.email);
    $('#ddlUserRole').val(user.userRoleID);     

    $('#upsertUser').modal(open);
}

function Delete(id) {
    $('#txtDeleteUserID').val(id);
    $('#deleteUser').modal(open);
}

