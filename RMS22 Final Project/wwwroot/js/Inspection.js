﻿var dataTable;
var inspectionData = {
    data: undefined
};

$(document).ready(function () {
    loadData().then(function () {
        loadDataTable();
    });
});


function loadData() {
    var getInspection = new Promise((resolve, reject) => {
        $.ajax({
            url: '/inspection/getall',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                inspectionData.data = data.data;
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        });
    });

    return Promise.all([getInspection]);
}

function loadDataTable() {
    dataTable = $('#DT_load').dataTable({
        "data": inspectionData.data,
        "scrollX": true,
        "columns": [  
            { "data": "propertyName" },
            { "data": "details" },
            { "data": "inspectionDate" },
            { "data": "inspectionStatus" },
            {
                "data": "inspectionID",
                "render": function (data) {

                    return `<div class="text-center">
                                <a class='btn btn-info text-white btn-sm' onclick=showItems(${data})> Items </a>
                                <a class='btn btn-success text-white btn-sm' onclick=Upsert(${data})> Edit </a>
                                <a class='btn btn-danger text-white btn-sm' onclick=Delete(${data})> Delete </a>
                            </div>`
                },
                "width": "175px"
            }
        ],
        "lanquage": {
            "emptyTable": "No Data"
        },
        "width": "100%"
    })
}

function Upsert(id) {
    var inspection = inspectionData.data.find(function (value) {
        return value.inspectionID == id
    }) || {
        active: true,
        details: '',
        inspectionDate: '', 
        inspectionStatus: 0,
        propertyID: 0
    }; 

    $('#txtDetails').val(inspection.details);
    $('#txtInspectionDate').val(inspection.inspectionDate.slice(0,10));
    $('#txtInspectionID').val(inspection.inspectionID);
    $('#chInspectionStatus').val(inspection.inspectionStatus);
  
    $('#ddlPropertyID').val(inspection.propertyID);
      

    $('#upsertInspection').modal(open);
}

function Delete(id) {
    $('#txtDeleteInspectionID').val(id);
    $('#deleteInspection').modal(open);
}

function showItems(id) {
    window.location.href = window.location.href + "/items/" + id;
}